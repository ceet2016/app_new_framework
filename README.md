# WebApp 开发框架【v3.0】

#### 介绍

**以下是前端 WebApp 框架的介绍和使用**

app_new_framework 是基于 Vue ，主框架为 Vant 的前端 WebApp 框架（同时支持 less，stylus语法）。

默认系统组件：

    1.主框架 Vant，辅框架 Mand Mobile
    2.网络请求主框架 Axios，辅框架 jQuery
    3.复制粘贴组件 clipper
    4.视频播放组件 vue-video-player
    4.上下滚动组件 vue-seamless-scroll
    5.多语言组件 VueI18n
    6.二维码生成组件 VueQriously
    7.其他略


框架位置介绍：

    1.build 插件控制文件夹【一般不必更改】

    2.config 项目启动以及打包路径设置
        a.dev.env.js 代理配置
        b.index.js 启动以及打包配置
        c.prod.env.js 生产环境下的接口配置

    3.dist 打包后文件所在目录

    4.src 代码目录
        a.assets 图片，视频，字体，基础样式文件所在目录
        b.components 代码所在目录，除开公用页面，其他模块页面均放在该模块内的文件夹中
        c.router Vue前端路由配置文件
        d.utils 常用的工具类，封装的常用的工具方法
        e.views 组件目录，封装的常用的组件【除非特殊情况，否则这里不应该被使用】
        f.App.vue App的基本文件【主要包含 app 热更新的代码，新项目必须修改此页面的内容，否则热更新将会出错】
        g.main.js 文件配置【引入等配置】

    5.static 静态资源目录
        a.css 皮肤文件，除非特殊情况这里只能拥有各种颜色文件
        b.lang 多语言包文件
        c.其他文件
    
    6.debug.log 调试文件【一般调试模式是除于关闭状态下的】

    7.index.html 页面入口文件【一般除了项目名以及项目图片不必做修改】

    8.常用命令.bat 打开它自然会看到怎么用

其他：

    debounce.js 的用法如下：
        1.引入文件
            import { debounce } from './../utils/debounce';
        
        2.在 created 方法中调用
            this.$watch('num', debounce((val) => { // 这里的 val 是输入的值
				if(val) { // 如果存在才执行
					this.getNum(val);
				}else{
					this.getNum(0);
				}
			}, 200));

    其他文件是使用方案，基本都是类似于这样的，自己猜吧，或者查看历史项目
            