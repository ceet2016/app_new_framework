import Vue from 'vue';
import Router from 'vue-router';
import Index from '@/components/Index';
import Home from '@/components/Home';

Vue.use(Router);

export default new Router({
  mode: 'hash',
  linkActiveClass: 'on',
  routes: [{
    path: '/',
    component: Index,
    children: [{
      path: '',
      name: 'Home',
      component: Home,
      meta: {
        title: '首页'
      }
    }, {
      path: '/Test',
      name: 'Test',
      component: () =>
        import('@/components/Test'),
      meta: {
        title: '第二个页面'
      }
    }]
  }, {
    path: '/Mine',
    name: 'Mine',
    component: () =>
      import('@/components/Mine'),
    meta: {
      title: '我的'
    }
  }, {
    path: '/Login',
    name: 'Login',
    component: () =>
      import('@/components/Login'),
    meta: {
      title: '登录'
    }
  }]
})
