/***
 * @param ceet@vip.qq.com
 * @param Toast显示时间封装
 */
import { Toast } from 'vant';
import 'vant/lib/index.css';

export function $toast(message, duration = 3000) {
	Toast({
		message,
		duration
	});
}