/***
 * @param ceet@vip.qq.com
 * @param func 输入完成的回调函数
 * @param delay 延迟时间
 * 使用方法：
 * 	this.$watch('num', debounce((val) => { // 这里的 val 是输入的值
 *		if(val) { // 如果存在才执行
 *			this.getNum(val);
 *		}else{
 *			this.getNum(0);
 *		}
 *	}, 200));
 */
export function debounce(func, delay) {
	let timer;
	return(...args) => {
		if(timer) {
			clearTimeout(timer);
		}
		timer = setTimeout(() => {
			func.apply(this, args);
		}, delay);
	}
}