import { get, post } from './http';

/**
 * @param {Base} [Base Api]
 */
export const base = {
	/**
	 * 帐号登录
	 * @param {POST} Method [请求的类型]
	 */
	getSignUp(res) {
		return post(process.env.API_HOST + 'login', res);
	}
}